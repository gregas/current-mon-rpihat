% RPi Current / Voltage / 1-Wire Board

RPi Current / Voltage / 1-Wire Board
====================================

This is a design for a Raspberry Pi B+ Hat featuring current and voltage 
monitoring and a 1-wire interface for temperature (or other) sensors.

Current and voltage are sensed by four INA209 (http://www.ti.com/lit/ds/symlink/ina209.pdf) 
ICs. These are meant to work in conjunction with current shunts.

An eight-channel interface for temperature sensors (and other 1-wire devices) is provided by
the Maxim DS2482-100 IC (https://www.maximintegrated.com/en/products/interface/controllers-expanders/DS2482-800.html)

The board also includes a CAT24C32 EEPROM (as required by Pi Hat spec).

The data sheets are inlcuded in this repository.